import sys
import cx_Oracle

TABLESPACE = r'''
            CREATE TABLESPACE SCRUMSYS
            LOGGING
            DATAFILE 'C:\oraclexe\app\oracle\oradata\XE\SCRUMSYS.dbf'
            SIZE 40M REUSE
            AUTOEXTEND ON NEXT 640 K MAXSIZE 100M
            '''

DBA_USER = '''
            create user SCRUMSYS identified by SCRUMSYS	
            DEFAULT TABLESPACE SCRUMSYS
            TEMPORARY TABLESPACE TEMP
            '''

DBA_GRANT = "GRANT DBA TO SCRUMSYS WITH ADMIN OPTION"

USER_ADMIN = "SYSTEM"

PASSWORD_ADMIN = "ariel"

HOST = r"localhost/XE"

# ------------------------------------------------------------------------------------------- #

connection = cx_Oracle.connect(USER_ADMIN, PASSWORD_ADMIN, HOST)
cursor = connection.cursor()
# Se elimina el tablespace en caso de que exista
try:
    print("ELIMINANDO USER Y TABLESPACE..")
    cursor.execute('DROP USER SCRUMSYS CASCADE')
    print("USER ELIMINADO..")
    cursor.execute('DROP TABLESPACE SCRUMSYS INCLUDING CONTENTS AND DATAFILES')
    print("TABLESPACE ELIMINADO..")
except cx_Oracle.DatabaseError as e:
    print("EL TABLESPACE O USUARIO ESTA SIENDO UTILIZADO. NO SE PUDO ELIMINAR..")
    print(str(e))
    sys.exit(1)

# ------------------------------------------------------------------------------------------- #

# Se crea el tablespace SCRUMSYS
try:
    print("CREANDO TABLESPACE SCRUMSYS..")
    cursor.execute(TABLESPACE)
    print("TABLESPACE CREADO..")

    print("CREANDO USUARIO..")
    cursor.execute(DBA_USER)
    print("USUARIO CREADO..")

    print("CREANDO PRIVILEGIOS ADMIN..")
    cursor.execute(DBA_GRANT)
    print("PRIVILEGIOS OTORGADOS..")
except cx_Oracle.DatabaseError as e:
    print(str(e))
finally:
    cursor.close()
    connection.close()
    print("CONEXION CERRADA..")

sys.exit(0)
