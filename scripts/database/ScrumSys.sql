/*==============================================================*/
/* DBMS name:      ORACLE Version 11g                           */
/* Created on:     17/mar./2019 10:50:59                        */
/*==============================================================*/



-- Type package declaration
create or replace package PDTypes  
as
    TYPE ref_cursor IS REF CURSOR;
end;

-- Integrity package declaration
create or replace package IntegrityPackage AS
 procedure InitNestLevel;
 function GetNestLevel return number;
 procedure NextNestLevel;
 procedure PreviousNestLevel;
 end IntegrityPackage;
/

-- Integrity package definition
create or replace package body IntegrityPackage AS
 NestLevel number;

-- Procedure to initialize the trigger nest level
 procedure InitNestLevel is
 begin
 NestLevel := 0;
 end;


-- Function to return the trigger nest level
 function GetNestLevel return number is
 begin
 if NestLevel is null then
     NestLevel := 0;
 end if;
 return(NestLevel);
 end;

-- Procedure to increase the trigger nest level
 procedure NextNestLevel is
 begin
 if NestLevel is null then
     NestLevel := 0;
 end if;
 NestLevel := NestLevel + 1;
 end;

-- Procedure to decrease the trigger nest level
 procedure PreviousNestLevel is
 begin
 NestLevel := NestLevel - 1;
 end;

 end IntegrityPackage;
/


drop trigger BI_US_SPRINT
/

alter table CRITERIOS_ACEPTACION
   drop constraint FK_CRITERIO_USTORY_CR_USER_STO
/

alter table SPRINTS
   drop constraint FK_SPRINTS_PROYECTO__PROYECTO
/

alter table USER_STORIES
   drop constraint FK_USER_STO_PROYECTO__PROYECTO
/

alter table USER_STORIES
   drop constraint FK_USER_STO_SPRINT_US_SPRINTS
/

alter table USUARIOS
   drop constraint FK_USUARIOS_PROYECTO__PROYECTO
/

alter table USUARIOS
   drop constraint FK_USUARIOS_ROL_USUAR_ROLES
/

drop index USTORY_CRITERIOACEPT_FK
/

drop table CRITERIOS_ACEPTACION cascade constraints
/

drop table PROYECTOS cascade constraints
/

drop table ROLES cascade constraints
/

drop index PROYECTO_SPRINT_FK
/

drop table SPRINTS cascade constraints
/

drop index SPRINT_USTORY_FK
/

drop index PROYECTO_USTORY_FK
/

drop table USER_STORIES cascade constraints
/

drop index PROYECTO_USUARIO_FK
/

drop index ROL_USUARIO_FK
/

drop table USUARIOS cascade constraints
/

/*==============================================================*/
/* Table: CRITERIOS_ACEPTACION                                  */
/*==============================================================*/
create table CRITERIOS_ACEPTACION 
(
   ID_CRITERIOACEPT     NUMBER               not null,
   ID_USTORY            NUMBER               not null,
   NOMBRE_CRITERIO      VARCHAR2(50)         not null,
   DESCRIPCION_CRITERIO VARCHAR2(300)        not null
)
/

alter table CRITERIOS_ACEPTACION
   add constraint PK_CRITERIOS_ACEPTACION primary key (ID_CRITERIOACEPT)
/

/*==============================================================*/
/* Index: USTORY_CRITERIOACEPT_FK                               */
/*==============================================================*/
create index USTORY_CRITERIOACEPT_FK on CRITERIOS_ACEPTACION (
   ID_USTORY ASC
)
/

/*==============================================================*/
/* Table: PROYECTOS                                             */
/*==============================================================*/
create table PROYECTOS 
(
   ID_PROYECTO          NUMBER               not null,
   NOMBRE_PROYECTO      VARCHAR2(50)         not null,
   FECHA_INICIO         DATE                 not null,
   FECHA_FIN_ESTIMADA   DATE                 not null
)
/

alter table PROYECTOS
   add constraint PK_PROYECTOS primary key (ID_PROYECTO)
/

/*==============================================================*/
/* Table: ROLES                                                 */
/*==============================================================*/
create table ROLES 
(
   ID_ROL               NUMBER               not null,
   NOMBRE_ROL           VARCHAR2(50)         not null,
   DESCRIPCION_ROL      VARCHAR2(200)
)
/

alter table ROLES
   add constraint PK_ROLES primary key (ID_ROL)
/

/*==============================================================*/
/* Table: SPRINTS                                               */
/*==============================================================*/
create table SPRINTS 
(
   ID_SPRINT            NUMBER               not null,
   ID_PROYECTO          NUMBER               not null,
   NOMBRE_SPRINT        VARCHAR2(50)         not null,
   DURACION             NUMBER               not null
)
/

alter table SPRINTS
   add constraint PK_SPRINTS primary key (ID_SPRINT)
/

/*==============================================================*/
/* Index: PROYECTO_SPRINT_FK                                    */
/*==============================================================*/
create index PROYECTO_SPRINT_FK on SPRINTS (
   ID_PROYECTO ASC
)
/

/*==============================================================*/
/* Table: USER_STORIES                                          */
/*==============================================================*/
create table USER_STORIES 
(
   ID_USTORY            NUMBER               not null,
   ID_PROYECTO          NUMBER               not null,
   ID_SPRINT            NUMBER,
   NOMBRE_USTORY        VARCHAR2(50)         not null,
   DESCRIPCION_USTORY   VARCHAR2(300)        not null,
   VALOR_NEGOCIO        NUMBER               default 0 not null,
   ESTADO               VARCHAR2(2)          default 'PH' not null,
   DURACION_ESTIMADA    NUMBER               not null
)
/

alter table USER_STORIES
   add constraint CKC_VALOR_NEGOCIO_USER_STO check (VALOR_NEGOCIO between 0 and 9)
/

alter table USER_STORIES
   add constraint CKC_ESTADO_USER_STO check (ESTADO in ('PH','EC','EP','HE'))
/

alter table USER_STORIES
   add constraint PK_USER_STORIES primary key (ID_USTORY)
/

/*==============================================================*/
/* Index: PROYECTO_USTORY_FK                                    */
/*==============================================================*/
create index PROYECTO_USTORY_FK on USER_STORIES (
   ID_PROYECTO ASC
)
/

/*==============================================================*/
/* Index: SPRINT_USTORY_FK                                      */
/*==============================================================*/
create index SPRINT_USTORY_FK on USER_STORIES (
   ID_SPRINT ASC
)
/

/*==============================================================*/
/* Table: USUARIOS                                              */
/*==============================================================*/
create table USUARIOS 
(
   ID_USUARIO           NUMBER               not null,
   ID_ROL               NUMBER,
   ID_PROYECTO          NUMBER,
   USERNAME             VARCHAR2(50)         not null unique,
   PASSWORD             VARCHAR2(50)         not null,
   NOMBRES              VARCHAR2(50)         not null,
   APELLIDOS            VARCHAR2(50)         not null,
   EMAIL                VARCHAR2(50)         not null,
   TELEFONO             VARCHAR2(50)         not null
)
/

alter table USUARIOS
   add constraint CKC_USERNAME_USUARIOS check (USERNAME = lower(USERNAME))
/

alter table USUARIOS
   add constraint PK_USUARIOS primary key (ID_USUARIO)
/

/*==============================================================*/
/* Index: ROL_USUARIO_FK                                        */
/*==============================================================*/
create index ROL_USUARIO_FK on USUARIOS (
   ID_ROL ASC
)
/

/*==============================================================*/
/* Index: PROYECTO_USUARIO_FK                                   */
/*==============================================================*/
create index PROYECTO_USUARIO_FK on USUARIOS (
   ID_PROYECTO ASC
)
/

alter table CRITERIOS_ACEPTACION
   add constraint FK_CRITERIO_USTORY_CR_USER_STO foreign key (ID_USTORY)
      references USER_STORIES (ID_USTORY) on delete cascade
/

alter table SPRINTS
   add constraint FK_SPRINTS_PROYECTO__PROYECTO foreign key (ID_PROYECTO)
      references PROYECTOS (ID_PROYECTO) on delete cascade
/

alter table USER_STORIES
   add constraint FK_USER_STO_PROYECTO__PROYECTO foreign key (ID_PROYECTO)
      references PROYECTOS (ID_PROYECTO) on delete cascade
/

alter table USER_STORIES
   add constraint FK_USER_STO_SPRINT_US_SPRINTS foreign key (ID_SPRINT)
      references SPRINTS (ID_SPRINT) on delete cascade
/

alter table USUARIOS
   add constraint FK_USUARIOS_PROYECTO__PROYECTO foreign key (ID_PROYECTO)
      references PROYECTOS (ID_PROYECTO) on delete cascade
/

alter table USUARIOS
   add constraint FK_USUARIOS_ROL_USUAR_ROLES foreign key (ID_ROL)
      references ROLES (ID_ROL) on delete cascade
/


create trigger BI_US_SPRINT before insert
on USER_STORIES for each row
declare
    integrity_error  exception;
    errno            number;
    errmsg           varchar2(200);
    p_id_proyecto number;

begin
    select id_proyecto into p_id_proyecto
    from sprints where id_sprint=:new.id_sprint;
    
    errno := '-20001';
    errmsg := 'El sprint no pertenece al proyecto'
    
    if p_id_proyecto != :new.id_proyecto then
        raise integrity_error;
    end if;
--  Errors handling
exception
    when integrity_error then
       raise_application_error(errno, errmsg);
end;
/

